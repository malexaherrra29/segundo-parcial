package facci.com.alexaherrera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.alexaherrera.R;

public class MainActivity extends AppCompatActivity {

Button botonRegistro, botonConsultarUsuario, botonConsultarListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ConexionSQLiteHelper conn= new ConexionSQLiteHelper(this, "bd_usuarios",null,1);

        botonRegistro = (Button)findViewById(R.id.btnRegistrarUsuario);
        botonConsultarUsuario = (Button)findViewById(R.id.btnConsultarUsuario);
        botonConsultarListView = (Button)findViewById(R.id.btnConsultarListView);

        botonConsultarListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Consultar_listView.class);
                startActivity(intent);
            }
        });



        botonRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, registro_activity.class);
                startActivity(intent);
            }
        });
        botonConsultarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Consulta_individual.class);
                startActivity(intent);
            }
        });

    }


}
